#!/bin/bash

source configs

tr "\n"

mkdir -p $tempFolder
rm $tempFolder/$step2_outfile
for EXT in `uniq $tempFolder/$step1_outfile | sed '/^$/d'`
do
  echo "searching directory/files with extension: $EXT using autocalibration"
	ffuf -w $directory -u $protocol://$host:$port/FUZZ -e "$EXT" -recursion -recursion-depth 2 -s -ac >> $tempFolder/$step2_outfile
	echo finish
done

#ONE COMMAND LINE VERSION:
#ffuf -w $extensions -u $protocol://$host:$port/indexFUZZ -s | ffuf -w $directory -u $protocol://$host:$port/FUZZ -e "$(cat)" -recursion -recursion-depth 2

